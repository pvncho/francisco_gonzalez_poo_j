import os
import random

class Pokemon:
    def __init__(self,nombrePokemon,identificadorPokemon,puntosVidaPokemon,puntosAtaquePokemon,tipoPokemon,debilidad,fortaleza):
        self.nombrePokemon=nombrePokemon
        self.identificadorPokemon=identificadorPokemon
        self.puntosVidaPokemon=puntosVidaPokemon
        self.puntosAtaquePokemon=puntosAtaquePokemon
        self.tipoPokemon=tipoPokemon
        self.debilidad=debilidad
        self.fortaleza=fortaleza

    def __str__(self):
        return(f'{self.nombrePokemon}\nID:{self.identificadorPokemon}\nHP:{self.puntosVidaPokemon}\nATK:{self.puntosAtaquePokemon}\nTIPO:{self.tipoPokemon}\nDebilidad:{self.debilidad}\nFortaleza:{self.fortaleza}\n')

    def __repr__(self):
        return(f'{self.nombrePokemon}')

    def atacar(self,Pokemon2):
        print(f'{self.nombrePokemon} ataca a {self.Pokemon2} con {self.puntosAtaquePokemon}')


    def autoRegeneracion(self,Pokemon2):
        self.puntosVidaPokemon+=self.puntosVidaPokemon*0.3
        Pokemon2.puntosVidaPokemon+=Pokemon2.puntosVidaPokemon*0.3

    def despedirse(self):
        print(f'*{self.nombrePokemon} hace ruidos de despedida*')

    def ventaja(self,Pokemon2):
        tiposPokemon =['Fuego','Agua','Hierba']
        for i,k in enumerate(tiposPokemon):
            if self.tipoPokemon == k:
                if Pokemon2.tipoPokemon == k:
                    Ataque1 = '\nNo es muy efectivo...'
                    Ataque2 = '\nNo es muy efectivo...'
            
                if Pokemon2.tipoPokemon == tiposPokemon[(i+1)%3]:
                    Pokemon2.puntosAtaquePokemon *= 0.3
                    self.puntosAtaquePokemon /= 0.2
                    Ataque1 = '\nNo es muy efectivo...'
                    Ataque2 = '\nEs muy efectivo!'

                if self.tipoPokemon == tiposPokemon[(i+2)%3]:
                    Pokemon2.puntosAtaquePokemon /= 0.2
                    self.puntosAtaquePokemon *= 0.3
                    Ataque1 = '\nEs muy efectivo!'
                    Ataque2 = '\nNo es muy efectivo...'
                return Ataque1, Ataque2
       


            
class tipoAgua(Pokemon):
    def __init__(self, nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusHidrobomba):
        super().__init__(nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusHidrobomba)
    def esquivaFuego(self):
        print(f'{self.nombrePokemon} reduce el daño recibido por ser tipo agua!')

class tipoFuego(Pokemon):
    def __init__(self, nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusRafagaFuego):
        super().__init__(nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusRafagaFuego)
        self.plusRafagaFuego=plusRafagaFuego

    def esquivaHierba(self):
        print(f'{self.nombrePokemon} reduce el daño recibido por ser tipo Fuego!')

class tipoHierba(Pokemon):
    def __init__(self, nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusAtaqueSolar):
        super().__init__(nombrePokemon, identificadorPokemon, puntosVidaPokemon, puntosAtaquePokemon, tipoPokemon, debilidad, fortaleza,plusAtaqueSolar)
    def esquivaAgua(self):
        print(f'{self.nombrePokemon} reduce el daño recibido por ser tipo Hierba!')

# Pokemones en memoria pokedex

if __name__ == '__main__': 
    Charmander = Pokemon('Charmander',5,100,50,'Fuego','Agua','Hierba')
    Squirtle = Pokemon('Squirtle',2,100,50,'Agua','Hierba','Fuego')
    Bulbasaur = Pokemon('Bulbasaur',1,100,50,'Hierba','Fuego','Agua')

# Módulo de duelos

def dueloPokemon(self, Pokemon2):
    turnoActual=0
    batallasGanadas=0
    print('Elige un pokemon de la siguiente lista')
    for x in pokemonesEnMemoria:
        print(repr(x))
    pokemonElejido=int(input('Ingresa el numero de pokemon que deseas'))
    print(f'Has escogido a {pokemonesEnMemoria[pokemonElejido]}')
    pokemonOponente=int(input('Elige un pokemon para tu oponente'))
    for x in pokemonesEnMemoria:
        print(repr(x))
    print(f'Has escogido a {pokemonesEnMemoria[pokemonOponente]}')

    moneda=random.choice(["Cara","Sello"])
    if moneda=='Cara':
        print('TU EMPIEZAS EL DUELO')
    else:
        print('TU OPONENTE EMPIEZA EL DUELO')
    
    print("-----DUELO POKEMON-----")
    print(f"\n{self.nombrePokemon}")
    print(f"Tipo/", self.tipoPokemon)
    print(f"Ataque/", self.puntosAtaquePokemon)
    print(f"HP/", self.puntosVidaPokemon)
    print(f"\nVS")
    print(f"\n{Pokemon2.nombrePokemon}")
    print(f"Tipo/", Pokemon2.tipoPokemon)
    print(f"Ataque/", Pokemon2.puntosAtaquePokemon)
    print(f"HP/", Pokemon2.puntosVidaPokemon)

    while (self.puntosVidaPokemon > 0) and (Pokemon2.puntosVidaPokemon > 0):
            Pokemon.ventaja(self,Pokemon2)
            print(f"Ve {self.nombrePokemon}!")
            comando1=int(input(f'Que debería hacer {self.nombrePokemon}\n1-Atacar\n2-Retirarse'))
            if comando1==1:
                Pokemon.atacar(self,Pokemon2)
                Pokemon2.puntosVidaPokemon -= self.puntosAtaquePokemon
                turnoActual+=1
                if Pokemon2.puntosVidaPokemon < 0:
                    print(f'{Pokemon2.nombrePokemon} ha muerto!')
                    batallasGanadas+=1
                    break
                print(f'A {Pokemon2.nombrePokemon} le quedan {Pokemon2.puntosVidaPokemon} restantes!')
            if comando1==2:
                Pokemon.despedirse(self)
                menuPokemon()
            comando2=int(input(f'Que deberia hacer {Pokemon2.nombrePokemon}?\n1-Atacar\n2-Retirarse'))
            if comando2==1:
                Pokemon.atacar(Pokemon2, self)
                self.puntosVidaPokemon -= Pokemon2.puntosAtaquePokemon
                turnoActual+=1
                if self.puntosVidaPokemon < 0:
                    print(f'{self.nombrePokemon} ha muerto!')
                    break


def menuPokemon():
    os.system('cls')
    entrenador=input('Ingresa tu nombre de entrenador\n')
    print('-----Bienvenido a Pokemon Text Duels!-----')
    print(f'Hola {entrenador}, elige una de las siguientes opciones: ')
    opcion=int(input('1-Pokedex\n2-Duelo Pokemon\n3-Estadisticas de batallas\n4-Salir\n'))
    if opcion == 1:
        print('Accediento a Pokedex...')
        menuPokedex()
    elif opcion == 2:
        print('Accediendo a módulo de duelos Pokemon')
        dueloPokemon()
    elif opcion == 3:
        print('Accediendo a estadísticas de duelos')
        print(f'NOMBRE ENTRENADOR: {entrenador}')
        print(f'Batallas ganadas: {batallasGanadas}')
        menuPokemon()
    elif opcion == 4:
        print('Saliendo de Pokemon Text Duels')
        quit()

batallasGanadas=0


### pokedex


def menuPokedex():
    print('-------POKEDEX-------')
    print('1-Ver Pokemons en memoria\n2-Agregar nuevo Pokemon\n3-Salir a menú principal\n')
    v=True
    while v:
        try:
            c = int(input('Ingrese el número de la opción que desea\n'))
        except ValueError:
            print('\nValor inválido')
            menuPokedex()
        if c == 1:
            verPokemon()
        elif c == 2:
            agregarPokemon()
        elif c == 3:
            salirAPrincipal()

def salirAPrincipal():
    print('Saliendo a menú principal\n')
    menuPokemon()

pokemonesEnMemoria = [Charmander,Bulbasaur,Squirtle]


def agregarPokemon():
    
    nuevoPokemon = Pokemon(
        input('Ingrese el nombre del pokemon\n'),
        int(input('Ingrese el numero de identificador del pokemon\n')),
        int(input('Ingrese los puntos de vida del Pokemon\n')),
        int(input('Ingrese los puntos de ataque del pokemon\n')),
        input('Ingrese el tipo del Pokemon\n'),
        input('Ingrese la debilidad del Pokemon\n'),
        input('Ingrese la fortaleza del Pokemon\n')
    )
    
    pokemonesEnMemoria.append(nuevoPokemon)
    print('Nuevo Pokemon registrado en la Pokedex\n')
    menuPokedex()
    return pokemonesEnMemoria

def verPokemon():
    print('------Pokemones registrados en Pokedex------\n')
    for x in pokemonesEnMemoria:
        print(x)
        print('XXXXXXXXXXXXXXXXXXXXXXX\n')
    menuPokedex()
            
menuPokemon()


            


                    

            
    



    


  
