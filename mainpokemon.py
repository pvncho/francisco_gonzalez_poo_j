import os
import random
from modelos import *


def menuPokemon():
    os.system('cls')
    entrenador=input('Ingresa tu nombre de entrenador')
    print('-----Bienvenido a Pokemon Text Duels!-----')
    print(f'Hola {entrenador}, elige una de las siguientes opciones: ')
    opcion=int(input('1-Pokedex\n2-Duelo Pokemon\n3-Estadisticas de batallas\n4-Salir\n'))
    if opcion == 1:
        print('Accediento a Pokedex...')
        menuPokedex()
    elif opcion == 2:
        print('Accediendo a módulo de duelos Pokemon')
    elif opcion == 3:
        print('Accediendo a estadísticas de duelos')
        print(f'NOMBRE ENTRENADOR: {entrenador}')
        print(f'Batallas ganadas: {batallasGanadas}')
        menuPokemon()
    elif opcion == 4:
        print('Saliendo de Pokemon Text Duels')
        quit()




### pokedex


def menuPokedex():
    print('-------POKEDEX-------')
    print('1-Ver Pokemons en memoria\n2-Agregar nuevo Pokemon\n3-Salir a menú principal\n')
    v=True
    while v:
        try:
            c = int(input('Ingrese el número de la opción que desea\n'))
        except ValueError:
            print('\nValor inválido')
            menuPokedex()
        if c == 1:
            verPokemon()
        elif c == 2:
            agregarPokemon()
        elif c == 3:
            salirAPrincipal()

def salirAPrincipal():
    print('Saliendo a menú principal\n')
    menuPokemon()

pokemonesEnMemoria = [Charmander]
batallasGanadas=0

def agregarPokemon():
    
    nuevoPokemon = Pokemon(
        input('Ingrese el nombre del pokemon\n'),
        int(input('Ingrese el numero de identificador del pokemon\n')),
        int(input('Ingrese los puntos de vida del Pokemon\n')),
        int(input('Ingrese los puntos de ataque del pokemon\n')),
        input('Ingrese el tipo del Pokemon\n'),
        input('Ingrese la debilidad del Pokemon\n'),
        input('Ingrese la fortaleza del Pokemon\n')
    )
    
    pokemonesEnMemoria.append(nuevoPokemon)
    print('Nuevo Pokemon registrado en la Pokedex\n')
    menuPokedex()
    return pokemonesEnMemoria

def verPokemon():
    print('------Pokemones registrados en Pokedex------\n')
    for x in pokemonesEnMemoria:
        print(x)
        print('XXXXXXXXXXXXXXXXXXXXXXX')
    menuPokedex()




